from django.shortcuts import render
from django.http import HttpResponse
from portfolio.models import FeedBack

def about(request):
    projects = FeedBack.objects.all()
    return render(request, 'contact/about.html',{'projects':projects})
def contact(request):
    return render(request, 'contact/contact.html')