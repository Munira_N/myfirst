from django.db import models

class FeedBack(models.Model):
    image=models.ImageField(upload_to='portfolio/images')
    name = models.CharField(max_length = 50)
    title = models.CharField(max_length = 50)
    text = models.CharField(max_length = 200)
