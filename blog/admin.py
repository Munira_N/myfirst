from django.contrib import admin

from .models import Blog

admin.site.register(Blog)

from .models import Recent

admin.site.register(Recent)

from .models import Instagram

admin.site.register(Instagram)
