from django.db import models

class Blog(models.Model):
    image = models.ImageField(upload_to='blog/image')
    data = models.DateField(auto_now=True)
    title = models.CharField(max_length=50)
    text = models.CharField(max_length=200)
    link = models.URLField()
class Recent(models.Model):
    image = models.ImageField(upload_to = 'blog/image')
    title = models.CharField(max_length=50)
    data = models.DateField(auto_now=True)
    link = models.URLField()

class Instagram(models.Model):
    image = models.ImageField(upload_to = 'blog/image')
    link = models.URLField(blank=True)

