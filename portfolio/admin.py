from django.contrib import admin

from .models import Service

admin.site.register(Service)

from .models import FeedBack

admin.site.register(FeedBack)