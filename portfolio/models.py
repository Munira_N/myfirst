from django.db import models

class FeedBack(models.Model):
    image=models.ImageField(upload_to='portfolio/images')
    name = models.CharField(max_length = 50)
    title = models.CharField(max_length = 50)
    text = models.CharField(max_length = 200)
   
class Service(models.Model):
    link = models.URLField()
    img1 = models.ImageField(upload_to='portfolio/images')
    img2 = models.ImageField(upload_to='portfolio/images')
    img3 = models.ImageField(upload_to='portfolio/images')
    img4 = models.ImageField(upload_to='portfolio/images')
    img5 = models.ImageField(upload_to='portfolio/images')
    img6 = models.ImageField(upload_to='portfolio/images')