from django.shortcuts import render
from django.http import HttpResponse
from .models import FeedBack
def base(request):
    return render(request, 'portfolio/base.html')
def home(request):
    projects = FeedBack.objects.all()
    return render(request, 'portfolio/home.html', {'projects':projects})
def portfolio(request):
    return render(request, 'portfolio/portfolio.html')
def services(request):
    return render(request, 'portfolio/services.html')   
